import React from 'react';
import { NavigatorIOS } from 'react-native';

import Home from './src/components/Home';

export default class App extends React.Component {
  render() {
    return (
        <NavigatorIOS
            style={{flex: 1}}
            initialRoute={{
              component: Home,
              title: 'SEFAZ Connect',
              backButtonTitle: 'Voltar',
              navigationBarHidden: true,
            }}/>
    );
  }
}