import React, {Component} from 'react';
import {Text, FlatList, View, AsyncStorage, StyleSheet, TouchableOpacity} from 'react-native';

export default class Notificacoes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: []
    };
  }

  async componentDidMount() {
    const novaSituacaoCnd = 'CN';
    const countRestricoes = 2;

    await AsyncStorage.getItem('@App:cnd', (error, result) => {
      if (result != null && result != novaSituacaoCnd) {
        var count = Object.keys(this.state.notifications).length + 1;
        this.setState({
          notifications: [...this.state.notifications, {position: count, key: 'A situação da sua CND mudou!'}]
        });
      }
    });

    await AsyncStorage.getItem('@App:restricoes', (error, result) => {
      // Simulating
      if (true || result != null && result != countRestricoes) {
        var count = Object.keys(this.state.notifications).length + 1;
        this.setState({
          notifications: [...this.state.notifications, {
            position: count,
            key: 'Houve atualização no número de restrições!'
          }]
        });
      }
    });

    // Load from SefazAPI
    var count = Object.keys(this.state.notifications).length + 1;
    const news1 = {
      position: count,
      key: '20/08/2017: Planalto vai fechar hoje acordo para novo REFIS.',
    };

    this.setState({
      notifications: [...this.state.notifications, news1]
    });
  }

  async onClear() {
    await AsyncStorage.removeItem('@App:cnd');
    await AsyncStorage.removeItem('@App:restricoes');
    await AsyncStorage.removeItem('@App:cnd');

    this.setState({notifications: []});
  }

  renderNotificationItem(item) {
    return (
        <TouchableOpacity style={styles.notification} key={item.key} onPress={() => this.onNotification(item)}>
          <Text style={styles.notificationText}>{item.position}. {item.key}</Text>
        </TouchableOpacity>
    );
  }

  render() {
    return (
        <View style={styles.container}>
          <Text style={styles.notificationLabel}>Notificações</Text>
          <FlatList data={this.state.notifications} renderItem={({item}) => this.renderNotificationItem(item)}/>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 18,
    backgroundColor: 'transparent',
  },
  notificationLabel: {
    textAlign: 'center',
    color: '#666',
    marginBottom: 10,
    fontWeight: 'bold',
  },
  notification: {
    margin: 0,
    padding: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
  notificationText: {
    color: '#333',
    marginBottom: 6,
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  }
});