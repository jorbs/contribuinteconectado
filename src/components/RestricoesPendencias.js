import React, {Component} from 'react';
import {Text, View, StyleSheet, AsyncStorage} from 'react-native';

import MyActivityIndicator from './MyActivityIndicator';
import * as SefazAPI from '../api/SefazAPI';

export default class RestricoesPendencias extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRequest: true,
      restricoes: [],
      pendenciasDebito: {},
      pendenciasParcelamento: {},
    };
  }

  componentDidMount() {
    SefazAPI.obterRestricoes(this.props.requestToken, this.props.login).then(restricoes => {
      var countMap = {};

      restricoes.forEach((restricao) => {
        countMap[restricao.descricaoRestricao] = 0;
      });

      restricoes.forEach((restricao) => {
        countMap[restricao.descricaoRestricao] += 1;
      });

      if (Object.keys(countMap).length !== 0) {
        this.setState({
          restricoes: countMap,
        });
      }
    }).then(() => {
      SefazAPI.consultarPendencias(this.props.requestToken, this.props.login, 'CACE').then(pendencias => {
        var debitoMap = {};
        var parcelamentoMap = {};

        pendencias.pendenciasDebito.forEach(pendencia => {
          debitoMap[pendencia.descricao] = 0;
        });

        pendencias.pendenciasDebito.forEach(pendencia => {
          debitoMap[pendencia.descricao] += 1;
        });

        pendencias.pendenciasParcelamento.forEach(pendencia => {
          parcelamentoMap[pendencia.descricao] = 0;
        });

        pendencias.pendenciasParcelamento.forEach(pendencia => {
          parcelamentoMap[pendencia.descricao] += 1;
        });

        this.setState({
          pendenciasDebito: debitoMap,
          pendenciasParcelamento: parcelamentoMap,
          pendingRequest: false
        });
      });
    });
  }

  render() {
    return (this.state.pendingRequest ?
            (<MyActivityIndicator/>) :
            (
                <View style={styles.container}>
                  <Text style={styles.section}>Restrições</Text>

                  {
                    this.state.restricoes.length === 0 ?
                        (<Text style={styles.noResults}>Nenhuma restrição foi encontrada.</Text>) :
                        (
                            Object.keys(this.state.restricoes).map((key) => {
                              <View key={key} style={styles.informationItems}>
                                <View style={styles.informationItem}>
                                  <Text>{key}</Text>
                                  <Text style={styles.downText}>Quantidade: {this.state.restricoes[key]}</Text>
                                </View>
                              </View>
                            })
                        )
                  }

                  <Text style={styles.section}>Pendências de Débito</Text>

                  {
                    Object.keys(this.state.pendenciasDebito).length === 0 ?
                        (<Text style={styles.noResults}>Nenhuma pendência foi encontrada.</Text>) :
                        (
                            Object.keys(this.state.pendenciasDebito).map((key) => {
                              return <View key={key} style={styles.informationItems}>
                                <View style={styles.informationItem}>
                                  <Text>{key}</Text>
                                  <Text style={styles.downText}>Quantidade: {this.state.pendenciasDebito[key]}</Text>
                                </View>
                              </View>;
                            })
                        )
                  }

                  <Text style={styles.section}>Pendências de Parcelamento</Text>

                  {
                    Object.keys(this.state.pendenciasParcelamento).length === 0 ?
                        (<Text style={styles.noResults}>Nenhuma pendência foi encontrada.</Text>) :
                        (
                            Object.keys(this.state.pendenciasParcelamento).map((key) => {
                              return <View key={key} style={styles.informationItems}>
                                <View style={styles.informationItem}>
                                  <Text>{key}</Text>
                                  <Text style={styles.downText}>Quantidade: {this.state.pendenciasParcelamento[key]}</Text>
                                </View>
                              </View>;
                            })
                        )
                  }
                </View>
            )
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  }
});