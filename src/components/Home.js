import React from 'react';
import {View, Button, StyleSheet, Text, TouchableOpacity, Image} from 'react-native';

import Login from './Login';
import Cadastro from './Cadastro';
import RestricoesPendencias from './RestricoesPendencias';
import ConsultaCND from './ConsultaCND';
import ConsultaTermos from './ConsultaTermos';
import ConsultaAntecipados from './ConsultaAntecipados';
import Processos from "./Processos";
import CallCenter from "./CallCenter";
import Notificacoes from "./Notificacoes";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  setCustomerInfo(login, requestToken) {
    this.setState({
      requestToken: requestToken,
      login: login
    });
  }

  onMenuItem(item) {
    if (this.state.login == null) {

    } else if (item == 'cadastro') {
      this.showCadastro();
    } else if (item == 'cnd') {
      this.showConsultaCND();
    } else if (item == 'termos') {
      this.showConsultaTermos();
    } else if (item == 'antecipados') {
      this.showConsultaAntecipados();
    } else if (item == 'restricoes') {
      this.showRestricoesPendencias();
    } else if (item == 'processos') {
      this.showProcessos();
    }
  }

  showLogin() {
    this.props.navigator.push({
      title: 'Login',
      component: Login,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login,
        setCustomerInfo: this.setCustomerInfo
      }
    });
  }

  showCadastro() {
    this.props.navigator.push({
      title: 'Dados Cadastrais',
      component: Cadastro,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    });
  }

  showRestricoesPendencias() {
    this.props.navigator.push({
      title: 'Restrições e Pendências',
      component: RestricoesPendencias,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    })
  }

  showConsultaCND() {
    this.props.navigator.push({
      title: 'Certidões',
      component: ConsultaCND,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    });
  }

  showConsultaTermos() {
    this.props.navigator.push({
      title: 'Termos de Apreensão',
      component: ConsultaTermos,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    });
  }

  showConsultaAntecipados() {
    this.props.navigator.push({
      title: 'Antecipados',
      component: ConsultaAntecipados,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    });
  }

  showProcessos() {
    this.props.navigator.push({
      title: 'Processos',
      component: Processos,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login,
        numeroProcesso: '5502-000011/2011'
      }
    });
  }

  showCallCenter() {
    this.props.navigator.push({
      title: 'Call Center',
      component: CallCenter,
      passProps: {
        requestToken: this.state.requestToken,
        login: this.state.login
      }
    });
  }

  showNotifications() {
    this.props.navigator.push({
      title: 'Notificações',
      component: Notificacoes
    });
  }

  render() {
    return (
        <View style={styles.container}>
          <Image style={styles.header} source={require('../img/fundo.png')}>
            <Image style={styles.logo} source={require('../img/logo.png')}/>
            {this.state.login ? <Notificacoes/> :
                <Login setCustomerInfo={(login, requestToken) => this.setCustomerInfo(login, requestToken)}/>}
          </Image>
          <Text style={styles.preIconsLabel}>Serviços para o Contribuinte</Text>
          <View style={styles.icons}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => this.onMenuItem('cadastro')}>
                <View style={styles.icon}>
                  <Image source={require('../img/portal-do-contribuinte.png')}/>
                  <Text style={styles.iconText}>Situação{"\n"}Cadastral</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onMenuItem('cnd')}>
                <View style={styles.icon}>
                  <Image source={require('../img/certidao-negativa.png')}/>
                  <Text style={styles.iconText}>Certidões</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onMenuItem('termos')}>
                <View style={styles.icon}>
                  <Image source={require('../img/documentos-de-arrecadacao.png')}/>
                  <Text style={styles.iconText}>Termos de{"\n"}Apreensão</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onMenuItem('restricoes')}>
                <View style={styles.icon}>
                  <Image source={require('../img/restricoes.png')}/>
                  <Text style={styles.iconText}>Restrições e{"\n"}Pendências</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => this.onMenuItem('antecipados')}>
                <View style={styles.icon}>
                  <Image source={require('../img/antecipado.png')}/>
                  <Text style={styles.iconText}>Antecipados</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onMenuItem('processos')}>
                <View style={styles.icon}>
                  <Image source={require('../img/processos.png')}/>
                  <Text style={styles.iconText}>Processos</Text>
                </View>
              </TouchableOpacity>
              {/*<TouchableOpacity onPress={() => this.onMenuItem('glpi')}>*/}
                {/*<View style={styles.icon}>*/}
                  {/*<Image source={require('../img/glpi.png')}/>*/}
                  {/*<Text style={styles.iconText}>Call Center</Text>*/}
                {/*</View>*/}
              {/*</TouchableOpacity>*/}
              <TouchableOpacity onPress={() => this.onMenuItem('simulador')}>
                <View style={styles.icon}>
                  <Image source={require('../img/simulador.png')}/>
                  <Text style={styles.iconText}>Simulador ST</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.icon}>
                  <Image source={require('../img/ncm.png')}/>
                  <Text style={styles.iconText}>NCM</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
    );

    return (
        <View style={styles.container}>
          <Button onPress={() => this.showLogin()} title="Login"/>
          <Button onPress={() => this.showCadastro()} title="Dados Cadastrais"/>
          <Button onPress={() => this.showRestricoesPendencias()} title="Restrições e Pendências"/>
          <Button onPress={() => this.showConsultaCND()} title="Certidões"/>
          <Button onPress={() => this.showConsultaTermos()} title="Termos de Apreensão"/>
          <Button onPress={() => this.showConsultaAntecipados()} title="Antecipados"/>
          <Button onPress={() => this.showProcessos()} title="Processos"/>
          <Button onPress={() => this.showCallCenter()} title="Call Center"/>
          <Button onPress={() => this.showNotifications()} title="Notificações"/>
        </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    // flex: 1,
    // flexDirection: 'row',
    // alignContent: 'center',
    marginTop: 22,
    paddingLeft: 10,
    paddingRight: 10
  },
  logo: {
    // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'center'
    // alignContent: 'center'
  },
  login: {
    flex: 1,
    marginTop: 5
  },
  preIconsLabel: {
    marginBottom: 4,
    textAlign: 'center',
    color: '#666',
    fontWeight: 'bold'
  },
  icons: {
    backgroundColor: '#efeff4',
    height: 240,
    paddingTop: 10,
    paddingBottom: 40,
    paddingLeft: 5,
    paddingRight: 10,
  },
  iconRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    alignItems: 'center',
    width: 76,
  },
  iconText: {
    fontSize: 12,
    marginTop: 3,
  }
});
