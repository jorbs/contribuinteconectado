import React, {Component} from 'react';
import {Text, TextInput, View, Alert, Switch, TouchableHighlight, ActivityIndicator} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';

const deviceName = 'iPhone de Joao';
const REMEMBER_ME_KEY = '@App:login';
const AUTHORIZATION_ID_KEY = '@App:idAutorizacao';
const REQUEST_TOKEN_KEY = '@App:requestToken';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pendingRequest: false,
      rememberMe: true,
      authorizationId: null,
      requestToken: null,
    }
  }

  authenticate() {
    SefazAPI.autenticar(this.state.login, this.state.authorizationId)
        .then(response => {
          if (response.hasOwnProperty('id_token')) {
            this.setState({
              requestToken: response.id_token,
              pendingRequest: false
            });
            this.props.setCustomerInfo(this.state.login, this.state.requestToken);
            // await AsyncStorage.setItem(REQUEST_TOKEN_KEY, this.state.requestToken);
          } else if (response.hasOwnProperty('mensagem')) {
            Alert.alert(response.mensagem);
          } else {
            Alert.alert('Não foi possível autenticar-se.');
          }
        });
  }

  componentDidMount() {
    this.setState({login: this.props.login});

    // await AsyncStorage.getItem(REMEMBER_ME_KEY, (error, result) => {
    //   if (result != null) {
    //     this.setState({login: result});
    //   }
    // });

    // await AsyncStorage.getItem(AUTHORIZATION_ID_KEY, (error, result) => {
    //   if (result != null) {
    //     this.authorizationId = result;
    //   }
    // });

    // await AsyncStorage.getItem(REQUEST_TOKEN_KEY, (error, result) => {
    //   if (result != null) {
    //     this.state.requestToken = result;
    //   }
    // });
  }

  toogleRememberMe(value) {
    this.setState({rememberMe: value});

    // if (!this.state.rememberMe) {
    //   await AsyncStorage.removeItem(REMEMBER_ME_KEY);
    // }
  }

  submitCredentials() {
    if (this.state.login == null || this.state.login.length === 0) {
      Alert.alert('Login inválido');
      return;
    }

    if (this.state.rememberMe) {
      // await AsyncStorage.setItem(this.rememberMeKey, this.state.login);
    }

    if (this.state.requestToken != null) {
      return;
    }

    this.setState({pendingRequest: true});

    if (this.state.authorizationId != null) {
      this.authenticate();
    } else {
      SefazAPI.solicitarAutorizacao(this.state.login, deviceName)
          .then(response => {
            console.log(response);
            if (response.hasOwnProperty('idAutorizacao')) {
              this.setState({authorizationId: response.idAutorizacao});
              this.authenticate();
            } else if (response.hasOwnProperty('mensagem')) {
              Alert.alert(response.mensagem);
            } else {
              Alert.alert('Não foi possível autorizar a apliação.');
            }

            this.setState({pendingRequest: false});
          });
    }
  }

  render() {
    return (
        <View style={{flex: 1, marginTop: 60, alignItems: 'center'}}>
          <View>
            <TextInput
                keyboardType='numeric'
                value={this.state.login}
                style={{height: 40, textAlign: 'center'}}
                placeholder="Digite o seu usuário"
                onChangeText={(value) => this.setState({login: value})}/>
          </View>
          <View>
            <TouchableHighlight
                style={{backgroundColor: '#c4225f', paddingTop: 6, paddingBottom: 6, paddingLeft: 12, paddingRight: 12}}
                accessibilityLabel="Acesse o Portal do Contribuinte"
                onPress={() => this.submitCredentials()}>
              <Text style={{color: 'blue', textAlign: 'center', color: 'white', fontSize: 18}}>Entrar</Text>
            </TouchableHighlight>
          </View>
          <View style={{flexDirection: 'row', marginTop: 15}}>
            <Switch
                value={this.state.rememberMe}
                onValueChange={(value) => this.toogleRememberMe(value)}
            />
            <Text style={{lineHeight: 26, marginLeft: 4, backgroundColor: 'transparent'}}>Lembrar acesso</Text>
          </View>
          <View>
            <ActivityIndicator animating={this.state.pendingRequest}/>
          </View>
        </View>
    );
  }
}