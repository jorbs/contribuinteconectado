import React, {Component} from 'react';
import {Text, Button, View, StyleSheet} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';
import MyActivityIndicator from './MyActivityIndicator';

export default class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRequest: true
    };
  }

  componentDidMount() {
    SefazAPI.obterContribuinte(this.props.requestToken, this.props.login).then(userInformation => {
      this.setState({
        userInformation,
        pendingRequest: false
      });
    });
  }

  render() {
    return (this.state.pendingRequest ?
            (<MyActivityIndicator/>) :
            (
                <View style={styles.container}>
                  <Text style={styles.section}>Dados Gerais</Text>

                  <View style={styles.informationItems}>
                    <View style={styles.informationItem}>
                      <Text>Razão Social</Text>
                      <Text style={styles.downText}>{this.state.userInformation.razaoSocial}</Text>
                    </View>

                    <View style={styles.informationItem}>
                      <Text style={styles.upperText}>Nome Fantasia</Text>
                      <Text style={styles.downText}>{this.state.userInformation.nomeFantasia}</Text>
                    </View>

                    <View style={styles.informationItem}>
                      <Text style={styles.upperText}>CNPJ</Text>
                      <Text style={styles.downText}>{this.state.userInformation.cnpj}</Text>
                    </View>

                    <View style={styles.informationItem}>
                      <Text style={styles.upperText}>Natureza Jurídica</Text>
                      <Text style={styles.downText}>{this.state.userInformation.naturezaJuridica}</Text>
                    </View>

                    <View style={styles.informationItem}>
                      <Text style={styles.upperText}>Situação Cadastral</Text>
                      <Text style={styles.downText}>{this.state.userInformation.descricaoSituacaoCadastral}</Text>
                    </View>
                  </View>

                  <Text style={styles.section}>Endereço</Text>

                  <View style={styles.informationItems}>
                    <View style={styles.informationItem}>
                      <Text>Logradouro</Text>
                      <Text style={styles.downText}>{this.state.userInformation.endereco}</Text>
                    </View>

                    <View style={styles.informationItem}>
                      <Text style={styles.upperText}>Telefone</Text>
                      <Text style={styles.downText}>{this.state.userInformation.numeroTelefone}</Text>
                    </View>
                  </View>
                </View>
            )
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  }
});