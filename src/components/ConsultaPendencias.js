import React, {Component} from 'react';
import {Text, Button, TextInput, View, Alert, ActivityIndicator} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';

export default class ConsultaPendencias extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRequest: true,
      cnd: null
    };
  }

  componentDidMount() {
    SefazAPI.consultarFronteiras(this.props.requestToken, this.props.login, 'CACE').then(cnd => {
      this.setState({
        cnd,
        pendingRequest: false
      });
    });
  }

  render() {
    return (
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <View>
            <Text>CND</Text>
          </View>
          <View>
            {
              this.state.pendingRequest ? (<ActivityIndicator />) : (
                  this.state.cnd == null ?
                      (<Text>Nenhuma CND foi encontrada.</Text>) :
                      (<Text>{this.state.cnd.tipoCertidao}</Text>)
              )
            }
          </View>
        </View>
    );
  }
}