import React, {Component} from 'react';
import {Text, Alert, TextInput, View, Clipboard, StyleSheet, TouchableHighlight, ActionSheetIOS} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';
import MyActivityIndicator from './MyActivityIndicator';

export default class ConsultaAntecipados extends Component {
  constructor(props) {
    super(props);

    let now = new Date();
    let month = now.getMonth().toString().length === 1 ? '0' + now.getMonth().toString() : now.getMonth().toString();
    // let competencia = month + '/' + now.getFullYear();
    let competencia = "08/2017";

    this.state = {
      competencia: competencia,
      antecipados: null,
      pendingRequest: false,
      modalVisible: false,
      month: now.getMonth().toString().length === 1 ? '0' + now.getMonth().toString() : now.getMonth().toString(),
      year: now.getFullYear(),
      rememberAntecipado: false
    };
  }

  onCopyClipboard(barCode) {
    Clipboard.setString(barCode);
    Alert.alert('Código de barras copiado!');
  }

  onConsultarValoresAntecipados() {
    this.setState({pendingRequest: true});

    SefazAPI.consultarValoresAntecipados(this.props.requestToken, this.props.login, this.state.competencia)
        .then(valoresAntecipados => valoresAntecipados.forEach((valorAntecipado) => {
              SefazAPI.consultarAntecipado(this.props.requestToken, this.props.login, valorAntecipado.sequencialAntecipacao)
                  .then(antecipados => {
                    this.setState({
                      antecipados,
                      pendingRequest: false
                    });
                  });
              this.setState({pendingRequest: false});
            })
        );
  }

  formatDate(date) {
    var day = date.substring(8, 10);
    var month = date.substring(5, 7);
    var year = date.substring(0, 4);

    return day + '/' + month + '/' + year;
  }

  onShare(antecipado) {
    ActionSheetIOS.showShareActionSheetWithOptions({
      url: 'https://www.dropbox.com/s/edjhee0jgvq263x/dar_hackathon.pdf',
      message: 'DAR Antecipado'
    }, () => {
      Alert.alert('Falha ao compartilhar o DAR.');
    }, () => {
      // Alert.alert('DAR   compartilhado com sucesso.');
    });
  }

  onRememberMe(vencimento) {
    var day = '15';
    var month = parseInt(vencimento.substring(5, 7)) + 1;
    var year = vencimento.substring(0, 4);

    Alert.alert(
        'Alerta de Vencimento',
        `Deseja criar evento no calendário para o dia ${day}/0${month}/${year}?`,
        [
          {
            text: 'Cancel', onPress: () => {
          }, style: 'cancel'
          },
          {text: 'OK', onPress: () => this.setState({rememberAntecipado: true})},
        ]
    );
  }

  renderRememberMe(antecipado) {
    return this.state.rememberAntecipado ?
        (
            <TouchableHighlight onPress={() => this.setState({rememberAntecipado: false})}>
              <Text style={styles.forgetEvent}>Esquecer evento de vencimento</Text>
            </TouchableHighlight>
        ) : (
            <TouchableHighlight onPress={() => this.onRememberMe(antecipado.dataVencimento)}>
              <Text style={styles.copyBarCodeButton}>Regitrar evento de vencimento</Text>
            </TouchableHighlight>
        );
  }

  renderAntecipado() {
    if (this.state.antecipados != null) {
      return this.state.antecipados.map((antecipado) => {
        return <View key={antecipado.numeroProcessamento}
                     style={{flex: 1, backgroundColor: 'white', padding: 10, marginTop: 20}}>
          <View style={styles.informationItems}>
            <View style={styles.informationItem}>
              <Text>Antecipado {antecipado.numeroProcessamento}</Text>
              <Text style={styles.downText}>Emissão: {this.formatDate(antecipado.dataEmissao)}</Text>
              <Text style={styles.downText}>Vencimento: {this.formatDate(antecipado.dataVencimento)}</Text>
              <Text style={styles.downText}>Código de Barras do DAR: {antecipado.codigoBarras.replace(/\ /g, '')}</Text>
              <Text style={styles.downText}>Valor Total do DAR: R$ {antecipado.valorTotal}</Text>

              <TouchableHighlight onPress={() => this.onCopyClipboard(antecipado.codigoBarras)}>
                <Text style={styles.copyBarCodeButton}>Copiar Código de Barras</Text>
              </TouchableHighlight>

              <TouchableHighlight onPress={() => this.onShare(antecipado)}>
                <Text style={styles.copyBarCodeButton}>Compartilhar DAR</Text>
              </TouchableHighlight>

              {this.renderRememberMe(antecipado)}
            </View>
          </View>
        </View>
      });
    } else {
      return null;
    }
  }

  render() {
    return (
        <View style={styles.container}>
          <Text style={styles.section}>Entre com a competência:</Text>

          <View style={{backgroundColor: 'white', padding: 10}}>
            <TextInput
                // defaultValue={this.state.competencia}
                defaultValue="02/2017"
                style={{textAlign: 'center', fontSize: 20, marginBottom: 10}}
                onChangeText={(value) => this.setState({competencia: value})}
            />

            <TouchableHighlight onPress={() => this.onConsultarValoresAntecipados()}>
              <Text style={styles.textButtonConsultar}>Consultar</Text>
            </TouchableHighlight>
          </View>

          {
            this.state.pendingRequest ? (<MyActivityIndicator/>) : (this.renderAntecipado())
          }
        </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  },
  textButton: {
    color: 'blue'
  },
  textButtonConsultar: {
    color: 'blue',
    textAlign: 'center'
  },
  copyBarCodeButton: {
    color: 'blue',
    marginTop: 5,
  },
  forgetEvent: {
    color: 'red',
    marginTop: 5,
  }
});