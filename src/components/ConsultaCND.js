import React, {Component} from 'react';
import {Text, Button, TextInput, View, StyleSheet, AsyncStorage} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';
import MyActivityIndicator from "./MyActivityIndicator";

export default class ConsultaCND extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRequest: true,
      cnd: null
    };
  }

  async componentDidMount() {
    const cnd = await SefazAPI.consultarCnd(this.props.requestToken, this.props.login, 'CACE');
    await AsyncStorage.setItem('@App:cnd', cnd.tipoCertidao);
    this.setState({ cnd, pendingRequest: false });
  }

  render() {
    return this.state.pendingRequest ?
        (<MyActivityIndicator/>) :
        (
            <View style={styles.container}>
              <Text style={styles.section}>{this.state.cnd.tipoCertidao == 'CP' ? 'Certidão Positiva' : 'Certidão Negativa'}</Text>

              <View style={styles.informationItems}>
                <View style={styles.informationItem}>
                  <Text>Número documento</Text>
                  <Text style={styles.downText}>{this.state.cnd.numeroDocumento}</Text>
                </View>
                <View style={styles.informationItem}>
                  <Text>Emissão</Text>
                  <Text style={styles.downText}>{this.state.cnd.dataEmissao} {this.state.cnd.horaEmissao}</Text>
                </View>
                <View style={styles.informationItem}>
                  <Text>Código de Autenticação</Text>
                  <Text style={styles.downText}>{this.state.cnd.codigoAutenticacao}</Text>
                </View>
              </View>
            </View>
        );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  }
});