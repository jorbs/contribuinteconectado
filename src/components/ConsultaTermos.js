import React, {Component} from 'react';
import {Text, Button, TextInput, View, Clipboard, StyleSheet, Modal, WebView, TouchableHighlight} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';
import MyActivityIndicator from './MyActivityIndicator';

export default class ConsultaTermos extends Component {
  constructor(props) {
    super(props);

    let now = new Date();
    let month = now.getMonth().toString().length === 1 ? '0' + now.getMonth().toString() : now.getMonth().toString();
    // let competencia = month + '/' + now.getFullYear();
    let competencia = "08/2017";

    this.state = {
      termos: [],
      competencia: competencia,
      pendingRequest: true,
      modalVisible: false
    };
  }

  componentDidMount() {
    SefazAPI.consultarTermosDeApreensao(this.props.requestToken, this.props.login, null, null).then(termos => {
      console.log(termos);
      this.setState({
        termos,
        pendingRequest: false
      });
    });
  }

  formatDate(date) {
    var day = date.substring(8, 10);
    var month = date.substring(5, 7);
    var year = date.substring(0, 4);

    return day + '/' + month + '/' + year;
  }

  render() {
    return (this.state.pendingRequest ?
            (<MyActivityIndicator/>) :
            (
                <View style={styles.container}>
                  <Text style={styles.section}>Termos de Apreensão</Text>

                  {
                    this.state.termos.length === 0 ?
                        (<Text style={styles.noResults}>Nenhum termo de apreensão foi encontrado.</Text>) :
                        (
                            this.state.termos.map((termo) => {
                              return <View key={termo.numeroTermo} style={styles.informationItems}>
                                <View style={styles.informationItem}>
                                  <Text>Termo {termo.numeroTermo}</Text>
                                  <Text style={styles.downText}>Status: {termo.status}</Text>
                                  <Text style={styles.downText}>Emissão: {this.formatDate(termo.dataEmissao)}</Text>
                                  <Text style={styles.downText}>Posto: {termo.posto}</Text>
                                  <TouchableHighlight onPress={() => this.setState({modalVisible: true})}>
                                    <Text style={styles.textButton}>Ver no Mapa</Text>
                                  </TouchableHighlight>
                                </View>
                              </View>;
                            })
                        )
                  }

                  <Modal
                      style={{paddingTop: 60}}
                      animationType={"slide"}
                      transparent={false}
                      visible={this.state.modalVisible}
                  >
                    <View style={{marginTop: 30, marginLeft: 10}}>
                      <TouchableHighlight onPress={() => this.setState({modalVisible: false})}>
                        <Text style={styles.textButton}>Voltar</Text>
                      </TouchableHighlight>
                    </View>
                    <WebView
                        source={{uri: 'http://sefaz.al.gov.br/mp-fisco'}}
                        style={{marginTop: 10}}
                    />
                  </Modal>
                </View>
            )
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  },
  textButton: {
    color: 'blue'
  },
  textButtonConsultar: {
    color: 'blue',
    textAlign: 'center'
  }
});