import React, {Component} from 'react';
import {Text, View, Alert, ActivityIndicator, StyleSheet} from 'react-native';

import * as SefazAPI from '../api/SefazAPI';
import MyActivityIndicator from "./MyActivityIndicator";

export default class Processos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRequest: true,
      processo: null
    };
  }

  componentDidMount() {
    SefazAPI.consultarPorNumeroProcesso(this.props.requestToken, this.props.numeroProcesso).then(processo => {
      console.log(processo);
      this.setState({
        processo: processo,
        pendingRequest: false
      });
    });
  }

  formatDate(date) {
    var day = date.substring(8, 10);
    var month = date.substring(5, 7);
    var year = date.substring(0, 4);

    return day + '/' + month + '/' + year;
  }

  render() {
    return this.state.pendingRequest ?
        (<MyActivityIndicator/>) :
        (
            <View style={styles.container}>
              <Text style={styles.section}>{this.state.processo.descricaoAssunto}</Text>

              <View style={styles.informationItems}>
                <View style={styles.informationItem}>
                  <Text>Órgão</Text>
                  <Text style={styles.downText}>{this.state.processo.descricaoOrgao}</Text>
                </View>
                <View style={styles.informationItem}>
                  <Text>Número Protocolo</Text>
                  <Text style={styles.downText}>{this.state.processo.numeroProtocolo}</Text>
                </View>
                <View style={styles.informationItem}>
                  <Text>Situação</Text>
                  <Text style={styles.downText}>{this.state.processo.situacao}</Text>
                </View>
                <View style={styles.informationItem}>
                  <Text>Última Movimentação</Text>
                  <Text style={styles.downText}>{this.formatDate(this.state.processo.ultimaMovimentacao)}</Text>
                </View>
              </View>
            </View>
        );

    return (
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <View>
            <Text>Processos</Text>
          </View>
          <View>
            {
              this.state.pendingRequest ? (<ActivityIndicator />) : (
                  this.state.processo == null ?
                      (<Text>Processo não encontrado.</Text>) :
                      (<Text>{this.state.processo.descricaoAssunto}</Text>)
              )
            }
          </View>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 65,
    backgroundColor: '#efeff4',
  },
  section: {
    marginTop: 15,
    marginLeft: 10,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  informationItems: {
    flex: 1,
    flexDirection: 'column',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: 'white',
  },
  informationItem: {
    flex: 1,
    flexDirection: 'column',
  },
  upperText: {
    marginTop: 5,
  },
  downText: {
    color: 'darkgray',
  },
  noResults: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    textAlign: 'center'
  }
});