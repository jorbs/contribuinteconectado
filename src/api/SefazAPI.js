const API = "http://hackathonapi.sefaz.al.gov.br";
const TOKEN_APP = "931731b3319055b9fea124e7ba3639d29ca5fd6c";
const AUTH_PREFIX = "Bearer ";

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

// ACESSO

export const autenticar = (login, authorizationId) =>
    fetch(`${API}/api/public/autenticar`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        login: login,
        idAutorizacao: authorizationId,
        tokenApp: TOKEN_APP
      })
    }).then(response => response.json())
        .then(data => data);

export const solicitarAutorizacao = (login, deviceName) =>
    fetch(`${API}/sfz-habilitacao-aplicativo-api/api/public/autorizacao-aplicativo/solicitar`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        login: login,
        nomeDispositivo: deviceName,
        tokenApp: TOKEN_APP
      })
    }).then(response => response.json())
        .then(data => data);

// CADASTRO

export const obterContribuinte = (requestToken, caceal) =>
    fetch(`${API}/sfz_cadastro_api/api/public/contribuinte/obterContribuinte/${caceal}`, {
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      }
    }).then(response => response.json())
        .then(data => data);

export const obterRestricoes = (requestToken, caceal) =>
    fetch(`${API}/sfz_cadastro_api/api/public/contribuinte/obterRestricoes/${caceal}`, {
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
    }).then(response => response.json())
        .then(restricoes => restricoes);

export const consultarCnd = (requestToken, numeroDocumento, tipoDocumento) =>
    fetch(`${API}/sfz_certidao_api/api/public/consultaCertidao/consultarCnd`, {
      method: 'POST',
      headers: {
        Autorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroDocumento,
        tipoDocumento
      })
    }).then(response => {
      console.log(response);
      return response.json();
    })
        .then(data => data);

export const consultarPendencias = (requestToken, numeroDocumento, tipoDocumento) =>
    fetch(`${API}/sfz_certidao_api/api/public/consultaCertidao/consultarPendencias`, {
      method: 'POST',
      headers: {
        Autorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroDocumento,
        tipoDocumento
      })
    }).then(response => response.json())
        .then(data => data.pendenciaDados);

// FRONTEIRAS

export const consultarTermosDeApreensao = (requestToken, numeroCaceal, periodoInicio, periodoTermino) =>
    fetch(`${API}/sfz_fronteiras_api/api/public/termoApreensao/consultar`, {
      method: 'POST',
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroCaceal,
        periodoInicio,
        periodoTermino
      })
    }).then(response => response.json())
        .then(data => data);

export const consultarValoresAntecipados = (requestToken, numeroCaceal, competencia) =>
    fetch(`${API}/sfz_fronteiras_api/api/public/antecipacao/consultarValoresAntecipados`, {
      method: 'POST',
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroCaceal,
        competencia
      })
    }).then(response => response.json())
        .then(data => data);

export const consultarAntecipado = (requestToken, numeroCaceal, sequencialAntecipacao) =>
    fetch(`${API}/sfz_fronteiras_api/api/public/dar/consultarAntecipado`, {
      method: 'POST',
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroCaceal,
        sequencialAntecipacao
      })
    }).then(response => response.json())
        .then(data => data);

// GLPI

export const abrirChamado = (requestToken, descricao, idCategoria, titulo) =>
    fetch(`${API}/sfz_fronteiras_api/api/public/dar/consultarAntecipado`, {
      method: 'POST',
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        descricao,
        idCategoria,
        titulo
      })
    }).then(response => response.json())
        .then(data => data);

export const listarChamados = (requestToken) =>
    fetch(`${API}/sfz_glpi_api/api/public/chamado`, {
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      }
    }).then(response => response.json())
        .then(data => data);

export const obterCategorias = (requestToken) =>
    fetch(`${API}/sfz_glpi_api/api/public/obterCategorias`, {
      method: 'GET',
      headers: {
        Autorization: AUTH_PREFIX + requestToken,
        ...headers
      }
    }).then(response => response.json())
        .then(data => data);

// PROCESSOS

export const consultarPorNumeroProcesso = (requestToken, numeroProcesso) =>
    fetch(`${API}/sfz_processo_api/api/public/processosAtivos/consultarPorNumeroProcesso`, {
      method: 'POST',
      headers: {
        Authorization: AUTH_PREFIX + requestToken,
        ...headers
      },
      body: JSON.stringify({
        numeroProcesso
      })
    }).then(response => response.json())
        .then(data => data);